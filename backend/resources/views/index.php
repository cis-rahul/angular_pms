<!DOCTYPE html>
<html lang="en" ng-app="myApp">

<head>
    <title></title>
    <script src="<?php echo asset('public/assets/libs/jquery.min.js') ?>"></script>
    <script src="<?php echo asset('public/assets/libs/angular.js')?>"></script>
    <script src="<?php echo asset('public/assets/libs/angular-ui-router.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo asset('public/app/components/frontend/frontend.module.js')?>"></script>
    <script type="text/javascript" src="<?php echo asset('public/app/components/administrator/products/product.module.js')?>"></script>
    <script type="text/javascript" src="<?php echo asset('public/app/components/administrator/categories/category.module.js')?>"></script>
    <script type="text/javascript" src="<?php echo asset('public/app/components/administrator/users/user.module.js')?>"></script>
    <script type="text/javascript" src="<?php echo asset('public/app/components/administrator/administrator.module.js')?>"></script>
    <script type="text/javascript" src="<?php echo asset('public/app.js') ?>"></script>
</head>

<body ui-view>
</body>

</html>
