var administratorApp = angular.module('administratorApp', ['ui.router', 'ui.router.compat', 'productApp', 'categoryApp', 'userApp']);
administratorApp.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('administrator', {
            url: '/administrator',
            views: {
                '': {
                    templateUrl: 'public/app/layouts/administrator/layout.html'
                },
                'header@administrator': {
                    templateUrl: 'public/app/layouts/administrator/header.html'
                },
                'sidebarLeft@administrator': {
                    templateUrl: 'public/app/layouts/administrator/sidebar-left.html'
                },
                'sidebarRight@administrator': {
                    templateUrl: 'public/app/layouts/administrator/sidebar-right.html'
                },
                'content@administrator': {
                    templateUrl: 'public/app/components/administrator/dashboard/index.html'
                },
                'footer@administrator': {
                    templateUrl: 'public/app/layouts/administrator/footer.html'
                }
            }
        })
        .state('administrator.dashboard', {
            url: '/dashboard',
            views: {
                'content@administrator': {
                    templateUrl: 'public/app/components/administrator/dashboard/index.html'
                }
            }
        })
        .state('administratorAuth', {
            url: '/administrator/auth',
            views: {
                '': {
                    templateUrl: 'public/app/layouts/administrator/auth/layout.html'
                },
                'content@administratorAuth': {
                    templateUrl: 'public/app/components/administrator/auth/login.html'
                }
            }
        })
        .state('administratorAuth.login', {
            url: '/login',
            views: {
                '': {
                    templateUrl: 'public/app/layouts/administrator/auth/layout.html'
                },
                'content@administratorAuth': {
                    templateUrl: 'public/app/components/administrator/auth/login.html'
                }
            }
        })
        .state('administratorAuth.register', {
            url: '/register',
            views: {
                'content@administratorAuth': {
                    templateUrl: 'public/app/components/administrator/auth/register.html'
                }
            }
        })
        .state('administratorAuth.forgotpassword', {
            url: '/forgot-password',
            views: {
                'content@administratorAuth': {
                    templateUrl: 'public/app/components/administrator/auth/forgotpassword.html'
                }
            }
        })
});
