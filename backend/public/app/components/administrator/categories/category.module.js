var categoryApp = angular.module('categoryApp', ['ui.router', 'ui.router.compat']);
categoryApp.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
            .state('administrator.category', {
                url: '/categories',
                views: {
                    'content@administrator': {
                        templateUrl: 'public/app/components/administrator/categories/views/index.html'
                    }
                }
            })
            .state('administrator.category.create', {
                url: '/create',
                views: {
                    'content@administrator': {
                        templateUrl: 'public/app/components/administrator/categories/views/create.html'
                    }
                }
            })
            .state('administrator.category.edit', {
                url: '/edit/:id',
                views: {
                    'content@administrator': {
                        templateUrl: 'public/app/components/administrator/categories/views/edit.html'
                    }
                }
            })
            .state('administrator.category.view', {
                url: '/:id',
                views: {
                    'content@administrator': {
                        templateUrl: 'public/app/components/administrator/categories/views/view.html'
                    }
                }
            })

});
