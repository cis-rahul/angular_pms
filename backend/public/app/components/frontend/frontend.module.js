var frontendApp = angular.module('frontendApp', ['ui.router', 'ui.router.compat', 'productApp']);
frontendApp.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('root', {
            url: '/',
            views: {
                '': {
                    templateUrl: 'public/app/layouts/frontend/layout.html'
                },
                'header@root': {
                    templateUrl: 'public/app/layouts/frontend/header.html'
                },
                'content@root': {                    
                    templateUrl: 'public/app/components/frontend/views/index.html'
                },
                'footer@root': {
                    templateUrl: 'public/app/layouts/frontend/footer.html'
                },
            }
        })
        .state('root.home', {
            url: 'home',
            views: {
                'content@root': {
                    templateUrl: 'public/app/components/frontend/views/index.html'
                }
            }
        })
        .state('root.about', {
            url: 'about',
            views: {
                'content@root': {
                    templateUrl: 'public/app/components/frontend/views/about.html'
                }
            }
        })
        .state('root.contact', {
            url: 'contact',
            views: {
                'content@root': {
                    templateUrl: 'public/app/components/frontend/views/contact.html'
                }
            }
        })
        .state('root.login', {
            url: 'login',
            views: {
                'content@root': {
                    templateUrl: 'public/app/components/frontend/views/login.html'
                }
            }
        })
        .state('root.register', {
            url: 'register',
            views: {
                'content@root': {
                    templateUrl: 'public/app/components/frontend/views/register.html'
                }
            }
        })
        .state('root.forgotpassword', {
            url: 'forgot-password',
            views: {
                'content@root': {
                    templateUrl: 'public/app/components/frontend/views/forgotpassword.html'
                }
            }
        })
        .state('root.terms', {
            url: 'terms-conditions',
            views: {
                'content@root': {
                    templateUrl: 'public/app/components/frontend/views/terms.html'
                }
            }
        })
        .state('root.rightSidebar', {
            url: 'right-sidebar',
            views: {
                'content@root': {
                    templateUrl: 'public/app/components/frontend/views/sidebar-right.html'
                }
            }
        })
        .state('root.leftSidebar', {
            url: 'left-sidebar',
            views: {
                'content@root': {
                    templateUrl: 'public/app/components/frontend/views/sidebar-left.html'
                }
            }
        })
});
